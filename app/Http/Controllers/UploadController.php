<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Http\Model\Product;
use Illuminate\Support\Facades\Input;
// use \XMLReader;
use \DOMDocument;

class UploadController extends Controller{
	public function upload()
	{
		$file = Input::file('Filedata');
		if($file -> isValid()){
			$realPath = $file -> getRealPath(); //temp file ab path
			$entension = $file -> getClientOriginalExtension();
			$newName = 'flux'.'.'.$entension;
			$path = $file -> move(base_path().'/files',$newName);
			//echo $path;

			$doc = new DOMDocument();
			$doc->load($path);
			$products = $doc->getElementsByTagName("product");
			foreach($products as $product){
				$id = $product->getElementsByTagName("id")->item(0)->nodeValue;
				$sku = $product->getElementsByTagName("sku")->item(0)->nodeValue;
				$ean = $product->getElementsByTagName("ean")->item(0)->nodeValue;
				$name = $product->getElementsByTagName("name")->item(0)->nodeValue;
				$stock = $product->getElementsByTagName("stock")->item(0)->nodeValue;
				$availability = $product->getElementsByTagName("availability")->item(0)->nodeValue;
				DB::table('product')->insert(
    						['id' => $id, 'sku' => $sku,
    						 'ean' => $ean, 'name' => $name,
    						 'stock' => $stock, 'availability' => $availability]
								);
				}
		}
		$productData = Product::get();
		return view('index')->with('data',$productData);
	}



}